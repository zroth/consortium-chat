//! Consortium Chat
//! Zach Roth 2023

use rand::prelude::SliceRandom;
use std::{
    env,
    error::Error,
    io::{self, BufRead},
};
use tokio::{
    io::{AsyncBufReadExt, AsyncWriteExt, BufReader},
    net::TcpStream,
};

const LOCAL_ADDRESS: &str = "127.0.0.1";

/// The main function for the chat client.
///
/// Serves as the entry point. It uses the `#[tokio::main]` attribute to run asynchronously using the Tokio runtime.
/// The function returns a `Result` indicating success or failure, with an empty `Ok` value or an error wrapped in a
/// `Box<dyn Error>`.
///
/// It starts by retrieving the port number from the command-line argument, or defaulting to port 8080 if not provided.
/// It then concatenates the local address with the port number to form the complete address.
///
/// It connects to a TCP server at the specified address using the `connect_to_server` function, then splits the TCP
/// stream into a reader and a writer using the `into_split` method. The reader is wrapped with a buffered reader using
/// `BufReader` for improved read performance.
///
/// Both a random emoji and username are generated for the client using the `generate_username` and `generate_emoji`
/// functions. A welcome message is displayed to the client using the `welcome_message` function.
///
/// An asynchronous task is spawned to read messages from the server using the `read_messages_from_server` function.
///
/// The function sends a user join message to the server by writing it to the writer; a newline character is sent to
/// indicate the end of the message.
///
/// User input is read from stdin, and the function uses `send_messages_to_server` to handle client input, sending messages
/// to the server through the writer.
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let port = env::args().nth(1).unwrap_or_else(|| "8080".to_string());
    let addr = format!("{}:{}", LOCAL_ADDRESS, port);

    let stream = connect_to_server(&addr).await?;
    let (reader, mut writer) = stream.into_split();

    let reader = BufReader::new(reader);

    let username = generate_username();
    let emoji = generate_emoji();

    welcome_message(&emoji, &username);

    tokio::spawn(read_messages_from_server(reader));

    writer
        .write_all(user_enter_chat(&emoji, &username).as_bytes())
        .await?;
    writer.write_all(b"\n").await?;

    let stdin = io::stdin();
    // Aquire lock on input stream, set up iterator over input
    let mut lines = stdin.lock().lines();
    send_messages_to_server(&mut lines, &mut writer, &emoji, &username).await?;

    Ok(())
}

/// Connects to a TCP server at the specified address or returns error if connection fails.
async fn connect_to_server(server_addr: &str) -> Result<TcpStream, Box<dyn Error>> {
    match TcpStream::connect(server_addr).await {
        Ok(stream) => Ok(stream),
        Err(err) => {
            eprintln!(
                "❌Failed to connect to the server! No server running on the requested port.❌"
            );
            Err(err.into())
        }
    }
}

/// Reads messages from the server and prints them to the console.
async fn read_messages_from_server(
    mut reader: tokio::io::BufReader<tokio::net::tcp::OwnedReadHalf>,
) {
    let mut line = String::new();
    loop {
        match reader.read_line(&mut line).await {
            // Server connection closed; exit loop
            Ok(0) => break,
            // Print read message
            Ok(_) => {
                let message = line.trim();
                println!("{}", message);
                line.clear();
            }
            // Server read error; exit loop
            Err(_) => break,
        }
    }
}

/// Sends messages from the client to the server.
async fn send_messages_to_server(
    lines: &mut io::Lines<io::StdinLock<'_>>,
    writer: &mut (impl AsyncWriteExt + Unpin),
    emoji: &str,
    username: &str,
) -> Result<(), Box<dyn Error>> {
    loop {
        if let Some(Ok(line)) = lines.next() {
            // User entered "quit"; exit loop
            if line.trim().to_lowercase() == "quit" {
                writer
                    // Send user left chat message to server
                    .write_all(user_left_chat(emoji, username).as_bytes())
                    .await?;
                writer.write_all(b"\n").await?;
                break;
            }

            let message = format!("{} {}: {}", emoji, username, line);
            writer.write_all(message.as_bytes()).await?;
            writer.write_all(b"\n").await?;
        }
    }
    Ok(())
}

/// Generates a username by randomly choosing from a predefined word list
pub fn generate_username() -> String {
    let word_list = vec![
        "abyss",
        "almond",
        "amethyst",
        "blossom",
        "blaze",
        "butterfly",
        "cactus",
        "caramel",
        "cascade",
        "diamond",
        "delight",
        "dusk",
        "effervescent",
        "emerald",
        "enigma",
        "falcon",
        "feather",
        "frost",
        "galaxy",
        "gazelle",
        "glimmer",
        "hazel",
        "harmony",
        "hurricane",
        "illusion",
        "indigo",
        "ivory",
        "jade",
        "jewel",
        "jubilee",
        "kaleidoscope",
        "karma",
        "koala",
        "labyrinth",
        "lighthouse",
        "luna",
        "mimosa",
        "mist",
        "mystic",
        "nebula",
        "nectar",
        "oasis",
        "opal",
        "onyx",
        "paradise",
        "peony",
        "penguin",
        "quartz",
        "quasar",
        "quench",
        "radiance",
        "raven",
        "ruby",
        "sapphire",
        "serene",
        "serenity",
        "thunder",
        "triumph",
        "twilight",
        "universe",
        "urchin",
        "utopia",
        "velvet",
        "vivid",
        "vortex",
        "whisper",
        "xanadu",
        "xenon",
        "yoga",
        "yonder",
        "zeppelin",
        "zenith",
    ];

    // Random number generator specific to current thread
    let mut rng = rand::thread_rng();

    // Build user name; "(0..2)" sets # of times operation will repeat
    let username: String = (0..2)
        // Defeference chosen random word from list, pass to map()
        .map(|_| *word_list.choose(&mut rng).unwrap())
        // Collect mapped values into vector
        .collect::<Vec<&str>>()
        // Concatenate vector elements w/ underscore
        .join("_");

    username
}

/// Generates a random emoji by choosing one emoji from a predefined emoji list
pub fn generate_emoji() -> String {
    let emoji_list = vec![
        "🌟", "🚀", "💡", "🔥", "🌈", "🐢", "🌺", "🌊", "🎉", "🍕", "🎸", "📚", "🌙", "⚡", "🍦",
        "🌸", "🌞", "🐳", "🌼", "🎻", "🎁", "🍔", "🎹", "🔒", "🌍", "🌩", "🍭", "🌹", "🌄", "🐬",
        "🌻", "💧", "🎈", "🌮", "🌹", "🔑", "🌎", "🌪", "🍩", "🌷", "🌅", "🦈", "🌧", "🎊", "🍟",
        "🎷", "🔓", "🌏", "⛈", "🍰", "🌇", "🐠", "🌺", "💨", "🎀", "🌭", "🎺", "🔐", "🌕", "🌧",
        "🍪", "🌆", "🐙", "💫", "🎵", "🍿", "🥁", "🗝️", "🌖", "🍨", "🌉", "🔒", "🎶", "🥤", "🎼",
        "🌗", "🌤️", "🍦", "🏞️", "🐌", "🌩️", "🎵", "🍺", "🪕", "🐝", "🌘", "🌥️", "🍩", "🏙️", "☀️",
    ];

    let mut rng = rand::thread_rng();
    // Choose random emoji from list
    (*emoji_list.choose(&mut rng).unwrap()).to_string()
}

/// Prints a welcome message with the provided emoji and username
pub fn welcome_message(emoji: &str, username: &str) {
    println!("\n");
    println!("╔═════════════════════════════════╗");
    println!("║     🦀Consortium Chat v1.0🦀    ║");
    println!("╚═════════════════════════════════╝");
    println!("Welcome to the chat!");
    println!("Your username is: {} {}", emoji, username);
    println!("Enter 'quit' to leave the chat");
    println!("\n");
}

/// Generates a message when a user enters the chat with the provided emoji and username
pub fn user_enter_chat(emoji: &str, username: &str) -> String {
    format!("🦀::: {} {} has entered the chat :::🦀", emoji, username)
}

/// Generates a message when a user leaves the chat with the provided emoji and username
pub fn user_left_chat(emoji: &str, username: &str) -> String {
    format!("🦀::: {} {} has left the chat :::🦀", emoji, username)
}

#[cfg(test)]
mod tests {
    use super::*;

    static EMOJI_LIST: &[&str] = &[
        "🌟", "🚀", "💡", "🔥", "🌈", "🐢", "🌺", "🌊", "🎉", "🍕", "🎸", "📚", "🌙", "⚡", "🍦",
        "🌸", "🌞", "🐳", "🌼", "🎻", "🎁", "🍔", "🎹", "🔒", "🌍", "🌩", "🍭", "🌹", "🌄", "🐬",
        "🌻", "💧", "🎈", "🌮", "🌹", "🔑", "🌎", "🌪", "🍩", "🌷", "🌅", "🦈", "🌧", "🎊", "🍟",
        "🎷", "🔓", "🌏", "⛈", "🍰", "🌇", "🐠", "🌺", "💨", "🎀", "🌭", "🎺", "🔐", "🌕", "🌧",
        "🍪", "🌆", "🐙", "💫", "🎵", "🍿", "🥁", "🗝️", "🌖", "🍨", "🌉", "🔒", "🎶", "🥤", "🎼",
        "🌗", "🌤️", "🍦", "🏞️", "🐌", "🌩️", "🎵", "🍺", "🪕", "🐝", "🌘", "🌥️", "🍩", "🏙️", "☀️",
    ];

    #[test]
    fn test_generate_username() {
        let username = generate_username();
        assert_eq!(username.split("_").count(), 2);
    }

    #[test]
    fn test_generate_emoji() {
        let emoji = generate_emoji();
        assert!(EMOJI_LIST.contains(&emoji.as_str()));
    }

    #[test]
    fn test_user_enter_chat() {
        let emoji = "🌟";
        let username = "test_user";
        let message = user_enter_chat(emoji, username);
        assert_eq!(message, "🦀::: 🌟 test_user has entered the chat :::🦀");
    }

    #[test]
    fn test_user_left_chat() {
        let emoji = "🌟";
        let username = "test_user";
        let message = user_left_chat(emoji, username);
        assert_eq!(message, "🦀::: 🌟 test_user has left the chat :::🦀");
    }
}
