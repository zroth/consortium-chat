# Zach Roth

# Consortium Chat Application

A place for fellow "Rustaceans" to find community, interact with others, and share common interests.

Consortium is a simple chat application built using Rust and the Tokio asynchronous runtime. It allows 
multiple clients to connect to a server and anonymously exchange messages in real-time. Each client is 
assigned a random username and emoji combination upon joining the chat. Clients can send messages to the 
server, which are then broadcasted to all other connected clients. The chat application provides a welcoming 
interface with instructions for users.

## Features

- 🦀 Server-client architecture
- 🦀 Broadcasting of messages to all connected clients
- 🦀 Randomly generated usernames and emojis for clients
- 🦀 Welcome message for clients upon connection
- 🦀 User entered/left chat notifications
- 🦀 Program termination commands for both server and client

## Requirements

- Rust programming language (stable version 1.68.2)
- `tokio` crate
- `rand` crate

## Installation

1. **Install Rust** by following the instructions at [Install Rust](https://www.rust-lang.org/tools/install).

Not sure if you already have Rust installed? Trying entering these commands:

```bash
rustc --version
```

or

```bash
cargo --version
```

2. **Clone the Consortium repository:**

   ```bash
   git clone https://gitlab.cecs.pdx.edu/zroth/consortium-chat.git
   ```

3. **Navigate to the project directory:**

   ```bash
   cd consortium-chat
   ```

4. **Build the project:**

   ```bash
   cargo build --release
   ```

5. **Start up the chat server:**

   If no user input is included, the server will default to port `8080`

   ```bash
   cargo run --bin server
   ```

   Otherwise, include the desired port #:

   ```bash
   cargo run --bin server 7000
   ```

6. **Start up a chat client:**

   ```bash
   cargo run --bin client
   ```

   By default (if no user input entered), the server will connect to port `8080`

   ```bash
   cargo run --bin client
   ```

   Otherwise, include the desired port #:

   ```bash
   cargo run --bin client 7000
   ```

   Note: if no server is currently listening on the requested port, you'll be notified with an error message.

Upon successful connection, you'll then be greeted by the server, given temporary user identifiers, and
given the ability to start sending and receiving messages.

To test this, open two or more seperate CLI, start up clients, then send and recieve messages between the clients

## Testing

The project includes a series of unit tests for some of the key helper functions of both the client and ther server. 
To run the tests, navigate to the project directory in the terminal and execute the command:

```bash
cargo test
```

## Example
A working demonstration of the program can be seen in the video presentation file included in the project repository;
it starts at 00:50 through the 03:40 mark.

## What did/did not work?

From the onset of the project, I started looking over the basics of the Tokio runtime via the docs.rs as well as 
Tokio's official website. I found some helpful online articles that dove deeper  into the core concepts and mechanisms 
powering Tokio's asynchronous execution model, shedding some light on how Tokio handles tasks, I/O operations, scheduling, 
and concurrency. From there I began initial attempts to get a chat server and client working working. Long story short, 
after a week of tinkering, troubleshooting, and constant complaints from the compiler, I could not make ground on getting 
message from client-to-client to display at all or display in the correct order. Needing to get a working program in place,
I opted towards a synchronous, multi-threaded approach using things like `thread` and `mpsc` from the standard library. 
After a few days I was able to get a basic back-and-forth between two clients working as desired. Still, I didn't want to 
give up on the async approach to building the application. I found a YouTube video that gave me a basic framework and from
there I continued to add more features and functionality as time allowed. However, my primary goal was to really understand
what was happening with the code and how components like `spawn`, `select`, and `broadcast::channel` actually worked. I 
abstracted the core functionality of both the client and server into smaller, helper functions, and implemeted a set of unit 
tests for the non-async helper functions. I wasn't able to get working unit tests for the async helper functions (I'd done 
some research on async testing options like tokio-test, but I'll some more time to play around). All in all, as frustrating
as it was from the start and as much as I would have loved to have worked out more from my project proposal, I had a ton of
fun buildling the project. It's increased my interested in networking applications and how to leverage the power and safety
of Rust! I do plan to continue building upon this project.

## Resources:

**Tokio Rust Docs:**

- [Module `io`](https://docs.rs/tokio/latest/tokio/io/index.html)
- [Module `net`](https://docs.rs/tokio/latest/tokio/net/index.html)
- [Module `sync`](https://docs.rs/tokio/latest/tokio/sync/index.html)
- [Macro `select!`](https://docs.rs/tokio/latest/tokio/macro.select.html)

**Tokio Tutorial:**

- [Spawning](https://tokio.rs/tokio/tutorial/spawning)
- [Shared State](https://tokio.rs/tokio/tutorial/shared-state)
- [Channels](https://tokio.rs/tokio/tutorial/channels)
- [I/O](https://tokio.rs/tokio/tutorial/io)
- [Select](https://tokio.rs/tokio/tutorial/select)

**Videos:**

- [Creating a Chat Server with async Rust and Tokio](https://www.youtube.com/watch?v=T2mWg91sx-o&t=718s)

**Articles:**

- [Async Rust: What is a runtime? Here is how tokio works under the hood](https://kerkour.com/rust-async-await-what-is-a-runtime)
- [Tokio internals: Understanding Rust's asynchronous I/O framework from the bottom up](https://cafbit.com/post/tokio_internals/)
